<?php

namespace Drupal\fb_conversion_commerce\EventSubscriber;

use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\fb_conversion\EventsRegistryInterface;
use Drupal\fb_conversion\EventSubscriber\FbConversionEventSubscriberBase;
use Drupal\fb_conversion\FacebookNormalizerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event handler for Kernel events.
 */
class KernelEventSubscriber extends FbConversionEventSubscriberBase {

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * Constructs the KernelEventsSubscriber object.
   *
   * @param \Drupal\fb_conversion\EventsRegistryInterface $events_registry
   *   The Facebook conversion API events registry.
   * @param \Drupal\fb_conversion\FacebookNormalizerInterface $facebook_normalizer
   *   The Facebook normalizer service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match, for context.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   */
  public function __construct(EventsRegistryInterface $events_registry, FacebookNormalizerInterface $facebook_normalizer, RouteMatchInterface $route_match, CheckoutOrderManagerInterface $checkout_order_manager) {
    parent::__construct($events_registry, $facebook_normalizer, $route_match);

    $this->checkoutOrderManager = $checkout_order_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // trackPageView should run before Dynamic Page Cache, which has priority
      // 27, see Drupal\dynamic_page_cache\EventSubscriber\DynamicPageCacheSubscriber.
      KernelEvents::REQUEST => ['trackInitCheckout', 28],
    ];
  }

  /**
   * Tracks the checkout event.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request.
   */
  public function trackInitCheckout(RequestEvent $event) {
    if (!$this->shouldTrackCheckout($event)) {
      return;
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->routeMatch->getParameter('commerce_order');
    if (!$order) {
      return;
    }

    $checkoutStepIndex = $this->getCheckoutStepIndex($order);
    // Only track checkout step 1 as InitiateCheckout event.
    if ($checkoutStepIndex === 1) {
      $event_data = [];
      if ($email = $order->getEmail()) {
        $email = $this->facebookNormalizer->normalizeEmail($email);
        $email = $this->facebookNormalizer->hash($email);
        $event_data['user_data'] = [];
        $event_data['user_data']['em'] = $email;
      }
      // @todo more details (content_ids,...).
      $this->eventsRegistry->registerEvent('InitiateCheckout', $event_data);
    }
  }

  /**
   * Check if the current request matches the conditions to track the checkout.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request.
   *
   * @return bool
   *   TRUE, if this request should be tracked as "checkout".
   */
  protected function shouldTrackCheckout(RequestEvent $event): bool {
    if ($this->routeMatch->getRouteName() !== 'commerce_checkout.form') {
      return FALSE;
    }

    // Bail if we are not dealing with a master request or GET method.
    if (!$event->isMainRequest() || !$event->getRequest()->isMethod('GET')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns an index for the current checkout step, starting at index 1.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return int
   *   Get the Checkout step number.
   */
  protected function getCheckoutStepIndex(OrderInterface $order): int {
    $checkoutFlow = $this->checkoutOrderManager->getCheckoutFlow($order);
    $checkoutFlowPlugin = $checkoutFlow->getPlugin();
    $steps = $checkoutFlowPlugin->getSteps();
    $requestedStepId = $this->routeMatch->getParameter('step');
    $currentStepId = $this->checkoutOrderManager->getCheckoutStepId($order, $requestedStepId);
    $currentStepIndex = array_search($currentStepId, array_keys($steps));

    if ($currentStepIndex === FALSE) {
      return 0;
    }

    return ++$currentStepIndex;
  }

}
