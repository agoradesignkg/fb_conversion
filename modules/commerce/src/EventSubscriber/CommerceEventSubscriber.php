<?php

namespace Drupal\fb_conversion_commerce\EventSubscriber;

use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\fb_conversion\EventSubscriber\FbConversionEventSubscriberBase;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event handler for commerce related events.
 */
class CommerceEventSubscriber extends FbConversionEventSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CartEvents::CART_ENTITY_ADD => 'trackCartAdd',
      'commerce_order.place.post_transition' => 'trackPurchase',
      // trackPageView should run before Dynamic Page Cache, which has priority
      // 27, see Drupal\dynamic_page_cache\EventSubscriber\DynamicPageCacheSubscriber.
      KernelEvents::REQUEST => ['trackProductView', 28],
    ];
  }

  /**
   * Track the "AddToCart" event.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The add to cart event.
   */
  public function trackCartAdd(CartEntityAddEvent $event) {
    $event_data = [];
    if ($email = $event->getCart()->getEmail()) {
      $email = $this->facebookNormalizer->normalizeEmail($email);
      $email = $this->facebookNormalizer->hash($email);
      $event_data['user_data'] = [];
      $event_data['user_data']['em'] = $email;
    }
    // @todo more details (content_ids,...).
    $this->eventsRegistry->registerEvent('AddToCart', $event_data);
  }

  /**
   * Track the "ViewContent" event.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function trackProductView(RequestEvent $event) {
    if ($event->getRequest()->getMethod() === 'GET' && $this->routeMatch->getRouteName() === 'entity.commerce_product.canonical') {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      $product = $this->routeMatch->getParameter('commerce_product');
      $default_variation = $product->getDefaultVariation();

      if ($default_variation) {
        // @todo more details (content_ids,...).
      }

      $this->eventsRegistry->registerEvent('ViewContent');
    }
  }

  /**
   * Track the "Purchase" event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function trackPurchase(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $event_data = [
      'custom_data' => [
        'currency' => $order->getTotalPrice()->getCurrencyCode(),
        'value' => $order->getTotalPrice()->getNumber(),
      ],
    ];
    // @todo more details (content_ids,...).
    $this->eventsRegistry->registerEvent('Purchase', $event_data);
  }

}
