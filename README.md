## Server-side configuration

Set `pixel_id` and `access_token` in fb_conversion.settings.

## GTM configuration

Set up datalayer variables accessing `fb_conversion.ViewContent` and if using Commerce, also these ones:
`fb_conversion.ViewContent`, `fb_conversion.AddToCart`, `fb_conversion.InitiateCheckout`, `fb_conversion.Purchase`.

Ensure that these variables are used as event IDs.
