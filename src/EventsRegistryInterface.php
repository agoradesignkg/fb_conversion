<?php

namespace Drupal\fb_conversion;

/**
 * Defines the Facebook events registry interface.
 */
interface EventsRegistryInterface {

  /**
   * Register the given event data for later sending.
   *
   * Missing mandatory user_data parameters will be autofilled, if possible.
   *
   * The event_id will always be set (overridden) here.
   *
   * The registered events info will also be used for clientside submission of
   * event IDs in GTM data layer.
   *
   * @param string $event_name
   *   The event name.
   * @param array $event_data
   *   The event data.
   * @param string|null $data_layer_name
   *   The variable name we provide for the Data Layer. If left NULL, the event
   *   name will be used.
   *
   * @see https://developers.facebook.com/docs/marketing-api/conversions-api/using-the-api
   */
  public function registerEvent(string $event_name, array $event_data = [], ?string $data_layer_name = NULL);

  /**
   * Get the registered event data, for sending to Facebook Conversion API.
   *
   * @return array
   *   The registered events.
   */
  public function getEvents(): array;

  /**
   * Get the registered event IDs (keyed by name) for GTM data layer.
   *
   * @param bool $purge
   *   Whether to purge the data. Defaults to TRUE.
   *
   * @return array
   *   The registered event IDs (keyed by name) for GTM data layer.
   */
  public function getGtmEventIds(bool $purge = TRUE): array;

}
