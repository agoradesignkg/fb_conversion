<?php

namespace Drupal\fb_conversion;

/**
 * Default Facebook normalizer implementation.
 */
class FacebookNormalizer implements FacebookNormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function hash(string $input): string {
    return hash('sha256', $input, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeEmail(string $input): string {
    return trim(strtolower($input), " \t\r\n\0\x0B.");
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeName(string $input): string {
    return preg_replace('/[^a-z]/', '', strtolower(trim($input)));
  }

}
