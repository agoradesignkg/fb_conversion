<?php

namespace Drupal\fb_conversion;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Default Facebook Conversion API client implementation.
 */
class ConversionApiClient implements ConversionApiClientInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Facebook events registry.
   *
   * @var \Drupal\fb_conversion\EventsRegistryInterface
   */
  protected $eventsRegistry;

  /**
   * Constructs a ConversionApiClient object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\fb_conversion\EventsRegistryInterface $events_registry
   *   The Facebook events registry.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, EventsRegistryInterface $events_registry) {
    $this->config = $config_factory->get('fb_conversion.settings');
    $this->httpClient = $http_client;
    $this->eventsRegistry = $events_registry;
  }

  /**
   * {@inheritdoc}
   */
  public function sendRequests() {
    $events = $this->eventsRegistry->getEvents();
    if (empty($events)) {
      return;
    }
    $events = array_values($events);
    $request_options = $this->getBasicRequestOptions();
    $request_options['form_params']['data'] = json_encode($events);
    try {
      $this->httpClient->request('POST', $this->getTargetUrl(), $request_options);
    }
    catch (GuzzleException $exception) {
      \Drupal::logger('fb_conversion')->error($exception->getMessage());
    }
  }

  /**
   * Returns the target url, already containing customized parameters.
   *
   * @return string
   *   The target url, already containing customized parameters.
   */
  protected function getTargetUrl(): string {
    return sprintf('https://graph.facebook.com/v%s/%s/events', $this->config->get('api_version'), $this->config->get('pixel_id'));
  }

  /**
   * Returns the basic request options, every request must include.
   *
   * Any call must provide an access token, which is set in the result.
   *
   * @return array
   *   The basic request options.
   */
  protected function getBasicRequestOptions(): array {
    $options = [
      'query' => [
        'access_token' => $this->config->get('access_token'),
      ],
    ];

    if ($test_event_code = $this->config->get('test_event_code')) {
      $options['test_event_code'] = $test_event_code;
    }

    return $options;
  }

}
