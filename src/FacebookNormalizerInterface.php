<?php

namespace Drupal\fb_conversion;

/**
 * Defines the Facebook normalizer interface.
 */
interface FacebookNormalizerInterface {

  /**
   * Hashes the given string.
   *
   * @param string $input
   *   The input string.
   *
   * @return string
   *   The hashed string.
   */
  public function hash(string $input): string;

  /**
   * Normalizes a string by lower-casing and trimming.
   *
   * @param string $input
   *   The input string.
   *
   * @return string
   *   The normalized string.
   */
  public function normalizeEmail(string $input): string;

  /**
   * Normalizes a name by lower-casing, trimming and guaranteeing Roman letters.
   *
   * @param string $input
   *   The input string.
   *
   * @return string
   *   The normalized string.
   */
  public function normalizeName(string $input): string;

}
