<?php

namespace Drupal\fb_conversion\EventSubscriber;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\fb_conversion\EventsRegistryInterface;
use Drupal\fb_conversion\FacebookNormalizerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Base class for our event subscribers.
 */
abstract class FbConversionEventSubscriberBase implements EventSubscriberInterface {

  /**
   * The Facebook conversion API events registry.
   *
   * @var \Drupal\fb_conversion\EventsRegistryInterface
   */
  protected $eventsRegistry;

  /**
   * The Facebook normalizer service.
   *
   * @var \Drupal\fb_conversion\FacebookNormalizerInterface
   */
  protected $facebookNormalizer;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs the FbConversionEventSubscriberBase object.
   *
   * @param \Drupal\fb_conversion\EventsRegistryInterface $events_registry
   *   The Facebook conversion API events registry.
   * @param \Drupal\fb_conversion\FacebookNormalizerInterface $facebook_normalizer
   *   The Facebook normalizer service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match, for context.
   */
  public function __construct(EventsRegistryInterface $events_registry, FacebookNormalizerInterface $facebook_normalizer, RouteMatchInterface $route_match) {
    $this->eventsRegistry = $events_registry;
    $this->facebookNormalizer = $facebook_normalizer;
    $this->routeMatch = $route_match;
  }

}
