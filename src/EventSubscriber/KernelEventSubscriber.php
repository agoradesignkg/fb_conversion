<?php

namespace Drupal\fb_conversion\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\fb_conversion\ConversionApiClientInterface;
use Drupal\fb_conversion\EventsRegistryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event handler for kernel events.
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The Facebook conversion API client.
   *
   * @var \Drupal\fb_conversion\ConversionApiClientInterface
   */
  protected $conversionApiClient;

  /**
   * The Facebook conversion API events registry.
   *
   * @var \Drupal\fb_conversion\EventsRegistryInterface
   */
  protected $eventsRegistry;

  /**
   * The admin routing context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs a ConversionApiClient object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\fb_conversion\ConversionApiClientInterface $conversion_api_client
   *   The Facebook conversion API client.
   * @param \Drupal\fb_conversion\EventsRegistryInterface $events_registry
   *   The Facebook conversion API events registry.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin routing context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConversionApiClientInterface $conversion_api_client, EventsRegistryInterface $events_registry, AdminContext $admin_context) {
    $this->config = $config_factory->get('fb_conversion.settings');
    $this->conversionApiClient = $conversion_api_client;
    $this->eventsRegistry = $events_registry;
    $this->adminContext = $admin_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // trackPageView should run before Dynamic Page Cache, which has priority
      // 27, see Drupal\dynamic_page_cache\EventSubscriber\DynamicPageCacheSubscriber.
      KernelEvents::REQUEST => ['trackPageView', 28],
      KernelEvents::FINISH_REQUEST => 'onFinishRequest',
    ];
  }

  /**
   * Sends the registered events to Facebook conversion API endpoint.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function trackPageView(RequestEvent $event) {
    if ($this->config->get('exclude_admin_pages') && $this->adminContext->isAdminRoute()) {
      return;
    }

    if ($event->isMainRequest()) {
      $this->eventsRegistry->registerEvent('PageView');
    }
  }

  /**
   * Sends the registered events to Facebook conversion API endpoint.
   *
   * @param \Symfony\Component\HttpKernel\Event\FinishRequestEvent $event
   *   The request.
   */
  public function onFinishRequest(FinishRequestEvent $event) {
    if ($event->isMainRequest()) {
      $this->conversionApiClient->sendRequests();
    }
  }

}
