<?php

namespace Drupal\fb_conversion;

/**
 * Defines the Facebook Conversion API client interface.
 */
interface ConversionApiClientInterface {

  /**
   * Sends the registered events to Facebook conversion API endpoint.
   */
  public function sendRequests();

}
