<?php

namespace Drupal\fb_conversion;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Default Facebook events registry implementation.
 */
class EventsRegistry implements EventsRegistryInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The Facebook normalizer.
   *
   * @var \Drupal\fb_conversion\FacebookNormalizerInterface
   */
  protected $facebookNormalizer;

  /**
   * The tempstore object for GTM event IDs.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $eventIdTempStore;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The uuid service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The event data.
   *
   * @var array
   */
  protected $events;

  /**
   * Constructs a EventsRegistry object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\fb_conversion\FacebookNormalizerInterface $facebook_normalizer
   *   The Facebook normalizer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user, RequestStack $request_stack, PrivateTempStoreFactory $temp_store_factory, UuidInterface $uuid, TimeInterface $time, FacebookNormalizerInterface $facebook_normalizer, ModuleHandlerInterface $module_handler) {
    $this->config = $config_factory->get('fb_conversion.settings');
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->currentUser = $current_user;
    $this->eventIdTempStore = $temp_store_factory->get('fb_conversion');
    $this->facebookNormalizer = $facebook_normalizer;
    $this->time = $time;
    $this->uuid = $uuid;
    $this->events = [];
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function registerEvent(string $event_name, array $event_data = [], ?string $data_layer_name = NULL) {
    if ($this->config->get('dnt')) {
      return;
    }

    if ($this->config->get('exclude_admin_roles')) {
      $admin_roles = \Drupal::entityQuery('user_role')
        ->condition('is_admin', TRUE)
        ->execute();

      if ($this->moduleHandler->moduleExists('agorabase')) {
        if ($moderator_role = agorabase_get_moderator_role()) {
          $admin_roles[$moderator_role] = $moderator_role;
        }
      }

      $admin_roles = array_values($admin_roles);
      $user_roles = $this->currentUser->getRoles(TRUE);
      if (!empty($user_roles) && array_intersect($admin_roles, $user_roles)) {
        return;
      }
    }

    $event_id = $this->uuid->generate();
    $event_data['event_name'] = $event_name;
    $event_data['event_id'] = $event_id;
    $event_data['event_time'] = $this->time->getRequestTime();

    if (empty($event_data['event_source_url'])) {
      $event_data['event_source_url'] = $this->currentRequest->getUri();
    }

    if (empty($event_data['action_source'])) {
      $event_data['action_source'] = 'website';
    }

    if (!isset($event_data['user_data'])) {
      $event_data['user_data'] = [];
    }

    if (empty($event_data['user_data']['em'])) {
      if ($this->currentUser->isAuthenticated() && !empty($this->currentUser->getEmail())) {
        $email = $this->facebookNormalizer->normalizeEmail($this->currentUser->getEmail());
        $email = $this->facebookNormalizer->hash($email);
        $event_data['user_data']['em'] = $email;
      }
    }

    if (empty($event_data['user_data']['client_ip_address'])) {
      $event_data['user_data']['client_ip_address'] = $this->currentRequest->getClientIp();
    }

    if (empty($event_data['user_data']['client_user_agent'])) {
      $event_data['user_data']['client_user_agent'] = $this->currentRequest->headers->get('User-Agent');
    }

    if (empty($event_data['user_data']['fbc'])) {
      if ($this->currentRequest->cookies->has('_fbc')) {
        $event_data['user_data']['fbc'] = $this->currentRequest->cookies->get('_fbc');
      }
    }

    if (empty($event_data['user_data']['fbp'])) {
      if ($this->currentRequest->cookies->has('_fbp')) {
        $event_data['user_data']['fbp'] = $this->currentRequest->cookies->get('_fbp');
      }
    }

    $this->events[$event_id] = $event_data;

    if (is_null($data_layer_name)) {
      $data_layer_name = $event_name;
    }

    $gtm_ids = $this->eventIdTempStore->get('gtm_event_ids');
    if (empty($gtm_ids)) {
      $gtm_ids = [];
    }
    $gtm_ids[$data_layer_name] = $event_id;
    $this->eventIdTempStore->set('gtm_event_ids', $gtm_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getEvents(): array {
    return $this->events;
  }

  /**
   * {@inheritdoc}
   */
  public function getGtmEventIds(bool $purge = TRUE): array {
    $gtm_ids = $this->eventIdTempStore->get('gtm_event_ids');
    if (empty($gtm_ids)) {
      return [];
    }
    if ($purge) {
      $this->eventIdTempStore->delete('gtm_event_ids');
    }
    return $gtm_ids;
  }

}
